import {ServerFactory} from "./common/server/ServerFactory";
import {FactoryInstantiator} from "./common/global/FactoryInstantiator";
import {DbConnectorFactory} from "./common/database/DbConnectorFactory";
import {ContainerBinder} from "./common/ioc/ContainerBinder";

// Need to bind classes for ioc
ContainerBinder.defaultBind();

// Instantiate needed factories, by configuration.
FactoryInstantiator.instantiateFactories();

const dbConnector = DbConnectorFactory.getInstance().getDbConnector();
const serverConnector = ServerFactory.getInstance().getServerConnector();

if (dbConnector && serverConnector) {
    serverConnector.setRoutes();
    dbConnector.connect();
    serverConnector.listen();
}


