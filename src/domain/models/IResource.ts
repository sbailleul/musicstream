export interface IResource {
    id: number;
    type: string;
    url: string;
}