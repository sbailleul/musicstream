import {Document, model, Model, Schema} from "mongoose";
import {ISchema} from "./ISchema";
import {ResourceDoc} from "./ResourceDoc";


export class ResourceSchema implements ISchema {

    createSchema(): Schema {
        return new Schema({
            id: {type: Number, unique: true},
            url: {type: String, required: true},
            type: {type: String, required: true}
        }, {versionKey: false});
    }

    getModel(): Model<Document> {
        return model<ResourceDoc>('resources', this.createSchema());
    }

}

