import {Document, Schema, Model} from "mongoose";

export interface ISchema {

    createSchema(): Schema;

    getModel():Model<Document>;

}
