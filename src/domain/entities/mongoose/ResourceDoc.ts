import {ResourceType} from "../ResourceType";
import {Document} from "mongoose";

export interface ResourceDoc extends Document {
    id: number;
    url: string;
    type: ResourceType;
}
