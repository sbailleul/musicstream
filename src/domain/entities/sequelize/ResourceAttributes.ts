
export interface ResourceAttributes {
    id: number;
    type: string;
    url: string;
}
