import Sequelize, {Model} from "sequelize";
import {IModel} from "./IModel";
import {ResourceType} from "../ResourceType";

export class SequelizeResourceModel extends Model{}

export class ResourceModel  implements IModel{

    init(sequelize: any) {
        SequelizeResourceModel.init({
                id: {type: Sequelize.INTEGER, primaryKey: true},
                type: Sequelize.ENUM({values: Object.keys(ResourceType)}),
                url: Sequelize.STRING
            }, {sequelize, tableName: 'resource', timestamps: false}
        );
    }

}

