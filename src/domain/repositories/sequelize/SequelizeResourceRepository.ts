import {SequelizeResourceModel} from "../../entities/sequelize/ResourceModel";
import {FindOptions} from "sequelize";
import {IResource} from "../../models/IResource";
import {IResourceRepository} from "../IResourceRepository";
import {injectable} from "../../../common/ioc/decorators";

@injectable
export class SequelizeResourceRepository implements IResourceRepository {


    delete(id: number): Promise<any> {
        return SequelizeResourceModel.destroy({
            where: {
                id
            }
        });
    }

    getAll(query: any): Promise<any> {
        return SequelizeResourceModel.findAll();
    }

    getOne(id: number): Promise<any> {
        return SequelizeResourceModel.findByPk(id);
    }

    insert(data: IResource): Promise<any> {
        return SequelizeResourceModel.create(data);
    }

    update(id: number, data: IResource): Promise<any> {
        return SequelizeResourceModel.update(
            {id : data.id, type: data.type, url: data.url},
            {returning: true, where: {id}}
        );
    }
}
