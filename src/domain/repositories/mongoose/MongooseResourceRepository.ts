import {MongooseRepository} from "./MongooseRepository";
import {ResourceSchema} from "../../entities/mongoose/ResourceSchema";
import {IResourceRepository} from "../IResourceRepository";
import {injectable} from "../../../common/ioc/decorators";

@injectable
export class MongooseResourceRepository extends MongooseRepository implements IResourceRepository {

    constructor() {
        super(new ResourceSchema());
    }
}
