import {Request, Response} from "express";
import {IRoutingData} from "../../common/router/IRoutingData";
import {constants} from "http2"
import {Router} from "../../common/router/Router";
import {IService} from "../../core/services/IService";

export abstract class Controller {

    protected abstract _service: IService;

    protected _routingData: IRoutingData[];

    constructor() {
        this.getOne = this.getOne.bind(this);
        this.getAll = this.getAll.bind(this);
        this.insert = this.insert.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
        this._routingData = [
            {url: '/', httpMethod: constants.HTTP2_METHOD_GET, callback: this.getAll},
            {url: '/', httpMethod: constants.HTTP2_METHOD_POST, callback: this.insert},
            {url: '/' + '/:id', httpMethod: constants.HTTP2_METHOD_GET, callback: this.getOne},
            {url: '/' + '/:id', httpMethod: constants.HTTP2_METHOD_PUT, callback: this.update},
            {url: '/' + '/:id', httpMethod: constants.HTTP2_METHOD_DELETE, callback: this.delete}
        ];
    }

    async getOne(req: Request, res: Response): Promise<any>  {
        const { id } = req.params;
        const entity = await this._service.getOne(id);

        if(entity == null){
            return res.status(404).send(entity);
        }
        return res.status(404).send(entity);
    }


    get routingData(): IRoutingData[] {
        return this._routingData;
    }

    async getAll(req: Request, res: Response): Promise<any>  {
        return res.status(200).send(await this._service.getAll(req.query));
    }

    async insert(req: Request, res: Response): Promise<any>  {
        const response = await this._service.insert(req.body);
        if (response.error){
            return res.status(response.statusCode).send(response);
        }
        return res.status(201).send(response);
    }

    async update(req: Request, res: Response): Promise<any>  {
        const { id } = req.params;

        const response = await this._service.update(id, req.body);

        return res.status(response.statusCode).send(response);
    }

    async delete(req: Request, res: Response): Promise<any>  {
        const { id } = req.params;

        const response = await this._service.delete(id);

        return res.status(response.statusCode).send(response);
    }

    protected changeBaseUrls(newBase: string) {
        this.routingData.forEach(route => route.url = Router.changeBasePath(route.url, newBase));
    }

}
