import {Controller} from "./Controller";
import {IResourceService} from "../../core/services/IResourceService";
import {CoreContainer} from "../../common/ioc/CoreContainer";
import {InjectableTypes} from "../../common/ioc/InjectableTypes";
import {injected} from "../../common/ioc/decorators";

export class ResourceController extends Controller {

    @injected(InjectableTypes.IResourceService)
    protected _service: IResourceService;

    constructor() {
        super();
        this.changeBaseUrls("/resources");
        CoreContainer.injectProperties(this);
    }
}
