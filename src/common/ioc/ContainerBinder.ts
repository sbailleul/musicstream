import {CoreContainer} from "./CoreContainer";
import {InjectableTypes} from "./InjectableTypes";
import {SequelizeConnection} from "../database/SequelizeConnection";
import {SequelizeResourceRepository} from "../../domain/repositories/sequelize/SequelizeResourceRepository";
import {ResourceService} from "../../core/services/ResourceService";
import {ExpressServer} from "../server/ExpressServer";

export class ContainerBinder {

    // Add type binding to ioc container
    public static defaultBind() {
        CoreContainer.bind(InjectableTypes.IDbConnection, SequelizeConnection);
        CoreContainer.bind(InjectableTypes.IServer, ExpressServer);
        CoreContainer.bind(InjectableTypes.IResourceRepository, SequelizeResourceRepository);
        CoreContainer.bind(InjectableTypes.IResourceService, ResourceService)
    }
}