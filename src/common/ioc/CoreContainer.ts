import "reflect-metadata";
import {InjectedProperty} from "./InjectedProperty";
import {InjectableTypes} from "./InjectableTypes";
import {ConstructorType} from "./ConstructorType";

export class CoreContainer {

    private static _typedProviders = new Map<InjectableTypes, ConstructorType<object>>();
    private static _injectables: ConstructorType<object>[] = [];
    private static _injectionTargets = new Map<string, InjectedProperty[]>();

    // Map type to inject with class constructor
    public static bind(type: InjectableTypes, provider: ConstructorType<object>) {
        if (!this.isInjectable(provider))
            return;
        if (this._typedProviders.has(type)) {
            console.error(`Type ${InjectableTypes[type]} already mapped`);
        } else {
            this._typedProviders.set(type, provider);
        }
    }

    // Add class constructor to _injectables
    public static addConcreteClass(classPrototype: ConstructorType<object>){
        this._injectables.push(classPrototype);
    }

    // Add type to inject for class property in _injectionTargets.
    public static addInjectedType(destClass: ConstructorType<object>, property: InjectedProperty){
        const className = destClass.name;
        if(this._injectionTargets.has(className) || !this._injectionTargets.get(className)){
            this._injectionTargets.set(className, []);
        }
        this._injectionTargets.get(className)!.push(property);
    }

    // Inject properties to class instance, look in _injectionTargets if class is referenced, if it's true try to find corresponding constructor to call to init property.
    public static injectProperties(instance: object){
        const className = instance.constructor.name;
        const propertiesToInject = this._injectionTargets.get(className);
        if(!propertiesToInject){
            console.error(`No properties to inject for ${className}`);
        }
        else {
            propertiesToInject.forEach(property => {
                const provider = this._typedProviders.get(property.type);
                if(provider){
                    Reflect.set(instance, property.name, new provider());
                }
                else {
                    console.error(`No provider of type ${InjectableTypes[property.type]} found for property ${property.name}`);
                }
            })
        }
    }

    // Test if class constructor exist in _injectables
    private static isInjectable( provider: ConstructorType<object>): boolean {
        return this._injectables.find(i => i.name === provider.name) !== undefined;
    }

}
