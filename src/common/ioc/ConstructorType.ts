// Type used to call class constructors
export type  ConstructorType<T> = new (...args: any[]) => T;