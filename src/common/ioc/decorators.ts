import {InjectableTypes} from "./InjectableTypes";
import {CoreContainer} from "./CoreContainer";
import {ConstructorType} from "./ConstructorType";


// Specify class properties are injected by ioc
export function injected(type: InjectableTypes): any {

    return function (target: any, propertyKey: string) {
        CoreContainer.addInjectedType(target.constructor, {name: propertyKey, type});
    }
}

// Specify than type are injectable by ioc
export function injectable(constructor: ConstructorType<object>) {
    CoreContainer.addConcreteClass(constructor);
}
