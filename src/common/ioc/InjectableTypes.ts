// List injectable types for ioc
export enum InjectableTypes {
    IDbConnection = 'IDbConnection',
    IServer = 'IServer',
    IResourceRepository = 'IResourceRepository',
    IResourceService = 'IResourceService',
}
