import {InjectableTypes} from "./InjectableTypes";

// Contain name and type of an injected property
export interface InjectedProperty {
    name: string;
    type: InjectableTypes
}