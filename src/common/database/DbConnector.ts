import {IDbConnection} from "./IDbConnection";
import {injectable, injected} from "../ioc/decorators";
import {InjectableTypes} from "../ioc/InjectableTypes";
import {CoreContainer} from "../ioc/CoreContainer";

// Use to connect application to database
@injectable
export class DbConnector {


    private readonly _connectionConfig: any;
    @injected(InjectableTypes.IDbConnection)
    private readonly _dbConnection: IDbConnection;

    constructor(connectionConfig: any) {
        this._connectionConfig = connectionConfig;
        CoreContainer.injectProperties(this);
    }

    get connectionConfig() {
        return this._connectionConfig;
    }


    get dbConnection(): IDbConnection {
        return this._dbConnection;
    }

    connect(){
        try{
            this.dbConnection.connect(this);
        } catch(err){
            throw err;
        }
    }

}
