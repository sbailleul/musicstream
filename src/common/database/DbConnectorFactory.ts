import {DbConnector} from "./DbConnector";
import {ResourcesConfig} from "../resources/ResourcesConfig";
import {DbConnectionType} from "./DbConnectionType";

export class DbConnectorFactory {

    private static _instance: DbConnectorFactory;
    private _dbConnector: DbConnector | undefined;
    private readonly _dbConnectionType: DbConnectionType;

    constructor(dbConnectorType: DbConnectionType) {
        this._dbConnectionType = dbConnectorType;
    }

    public static getInstance(dbConnectorType?: DbConnectionType): DbConnectorFactory{

        if(!this._instance && dbConnectorType){
            this._instance = new DbConnectorFactory(dbConnectorType);
        }
        return this._instance;
    }

    public getDbConnector(): DbConnector|undefined {

        if(!this._dbConnector){
            switch (this._dbConnectionType) {
                case DbConnectionType.SEQUELIZE:
                    this._dbConnector = new DbConnector(ResourcesConfig.SEQUELIZE_CONFIG);
                    break;
                case DbConnectionType.MONGOOSE:
                    this._dbConnector = new DbConnector(ResourcesConfig.MONGOOSE_CONFIG);
                    break;
            }
        }
        return this._dbConnector;
    }
}
