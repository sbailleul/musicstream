import {IDbConnection} from "./IDbConnection";
import {DbConnector} from "./DbConnector";
import {Sequelize} from 'sequelize';
import {injectable} from "../ioc/decorators";
import {ResourceModel} from "../../domain/entities/sequelize/ResourceModel";

@injectable
export class SequelizeConnection implements IDbConnection {

    private readonly _modelArray = [
        new ResourceModel()
    ];

    private _sequelize: Sequelize | undefined;

    connect(dbConnector: DbConnector): boolean {

        const connectionConfig: any = dbConnector.connectionConfig;

        this._sequelize = new Sequelize(connectionConfig.db, connectionConfig.user, connectionConfig.password, {
            host: connectionConfig.host,
            dialect: connectionConfig.dialect
        });
        this.initModels();
        return true;
    }

    private initModels(){
        this._modelArray.forEach(model => model.init(this._sequelize));
    }
}
