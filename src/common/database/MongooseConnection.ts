import {IDbConnection} from "./IDbConnection";
import {DbConnector} from "./DbConnector";

export class MongooseConnection  implements IDbConnection{

    private _mongoose : any;
    constructor(){
        this._mongoose = require('mongoose');
    }

    connect(dbConnector: DbConnector): boolean {
        try{
            this._mongoose.connect(this.createConnectionString(dbConnector), {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useCreateIndex: true} );
        } catch(err) {
            throw err;
        }
        return true;
    }

    createConnectionString(dbConnector: DbConnector): string {

        const connectionString = new URL("mongodb://");
        const connectionConfig: any = dbConnector.connectionConfig;
        connectionString.host = connectionConfig.host;
        connectionString.username = connectionConfig.user;
        connectionString.password = connectionConfig.password;
        connectionString.pathname = connectionConfig.db;

        return connectionString.toString();
    }

}
