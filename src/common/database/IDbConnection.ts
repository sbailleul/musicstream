import {DbConnector} from "./DbConnector";

//Db connection to communicate with databases SQL or MongoDB for now
export interface IDbConnection {

    connect(dbConnector: DbConnector):boolean;
}
