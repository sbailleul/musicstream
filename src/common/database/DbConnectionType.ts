// Use to know which db connection type to use
export enum DbConnectionType {
    SEQUELIZE = 'SEQUELIZE',
    MONGOOSE = 'MONGOOSE'
}
