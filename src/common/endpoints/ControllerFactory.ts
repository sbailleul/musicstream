import {Controller} from "../../web/controllers/Controller";
import {ConstructorType} from "../ioc/ConstructorType";
import {ResourceController} from "../../web/controllers/ResourceController";

export class ControllerFactory {

    private static _instance: ControllerFactory;
    private readonly _controllerSuffix = "Controller";
    private readonly _controllersArray: Controller[];
    private readonly _constructors = new Map<string, ConstructorType<object>>([
        [ResourceController.name, ResourceController]
    ]);

    private constructor() {
        this._controllersArray = [];
    }

    public static getInstance(): ControllerFactory {
        if (!this._instance) {
            this._instance = new ControllerFactory();
        }
        return this._instance;
    }

    public getController(controllerType: string): Controller | undefined {

        if (!controllerType) {
            return undefined;
        }

        let controller: Controller | undefined;
        const controllerName = controllerType + this._controllerSuffix;

        if (this._controllersArray) {
            controller = this._controllersArray.find(controller => controller.constructor.name === controllerType.constructor.name);
        }
        if (!controller) {
            const controllerClass: ConstructorType<object> | null = this.getControllerByName(controllerName);
            controller = (new controllerClass!() as Controller);
            this._controllersArray.push(controller);
        }
        return controller;
    }

    private getControllerByName(name: string): ConstructorType<object> | null {
        const constructor = this._constructors.get(name);
        return constructor !== undefined ? constructor : null;
    }


}
