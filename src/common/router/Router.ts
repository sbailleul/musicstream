import {ControllerFactory} from "../endpoints/ControllerFactory";
import {IServer} from "../server/IServer";
import {RestType} from "../endpoints/RestType";
import {ObjectUtil} from "../utils/ObjectUtil";

export class Router{


    public static setRoutes(server: IServer) {
        Object.keys(RestType).forEach(key => this.setRouteByRestType(server , ObjectUtil.getObjectValueByKey(RestType,key)));
    }

    public static changeBasePath(url: string, newBase: string ):string{
        const path = url.split("/");

        if(url[0] === '/'){
            path[1] = newBase;
        } else {
            path[0] = newBase;
        }

        const filteredPath = path.filter(substring => substring.trim() !== '');
        const newUrl = filteredPath.join("/");
        return newUrl;
    }

    private static setRouteByRestType(server: IServer, restType: string) : void{

        const controller =  ControllerFactory.getInstance().getController(restType)

        if(!restType || !server || !controller){
            return;
        }
        controller.routingData.forEach(route => server.route(route));
    }


}
