import {Request, Response} from "express";

export interface IRoutingData{
    url: string;
    httpMethod: string;
    callback:(req: Request, res: Response)=> Promise<any>;
}