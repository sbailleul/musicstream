import {ResourcesConfig} from "../resources/ResourcesConfig";
import {ObjectUtil} from "../utils/ObjectUtil";
import {DbConnectionType} from "../database/DbConnectionType";
import {ServerType} from "../server/ServerType";
import {DbConnectorFactory} from "../database/DbConnectorFactory";
import {ServerFactory} from "../server/ServerFactory";

export class FactoryInstantiator{

    public static instantiateFactories(){

        const dbConnectorType: DbConnectionType = ObjectUtil.getObjectValueByKey(DbConnectionType,ResourcesConfig.GLOBAL_CONFIG.dbConnectorType);
        const serverType: ServerType = ObjectUtil.getObjectValueByKey(ServerType,ResourcesConfig.GLOBAL_CONFIG.serverType);

        DbConnectorFactory.getInstance(dbConnectorType);
        ServerFactory.getInstance(serverType);
    }

}
