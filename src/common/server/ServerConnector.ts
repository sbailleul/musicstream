import {IServer} from "./IServer";
import {Router} from "../router/Router";
import {injected} from "../ioc/decorators";
import {InjectableTypes} from "../ioc/InjectableTypes";
import {CoreContainer} from "../ioc/CoreContainer";

export class ServerConnector {

    private readonly _port: number;
    @injected(InjectableTypes.IServer)
    private readonly _server: IServer;


    constructor(data: { port: number }) {
        this._port = data.port;
        CoreContainer.injectProperties(this);
    }


    get port(): number {
        return this._port;
    }

    listen(){
        this._server.listen(this);
    }

    setRoutes(){
        Router.setRoutes(this._server);
    }
}
