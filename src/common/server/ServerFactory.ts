import {ServerConnector} from "./ServerConnector";
import {ServerType} from "./ServerType";
import {ResourcesConfig} from "../resources/ResourcesConfig";

export class ServerFactory {

    private static instance: ServerFactory;
    private _serverConnector: ServerConnector | undefined;
    private readonly serverType: ServerType;

    constructor(serverType: ServerType) {
        this.serverType = serverType;
    }

    public static getInstance(serverType?: ServerType): ServerFactory{

        if(!this.instance && serverType){
            this.instance = new ServerFactory(serverType);
        }
        return this.instance;
    }

    public  getServerConnector(): ServerConnector {

        switch (this.serverType) {
            case ServerType.EXPRESS:
                this._serverConnector = new ServerConnector(ResourcesConfig.EXPRESS_CONFIG);
                break;
        }

        return this._serverConnector;
    }
}
