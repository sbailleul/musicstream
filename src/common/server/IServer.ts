import {ServerConnector} from "./ServerConnector";
import {Request, Response} from 'express';

export interface IServer{

    listen(serverConnector: ServerConnector): void;
    route(data: {url: string, httpMethod: string, callback:(req: Request, res: Response)=> Promise<any>}): void;

}
