import {IServer} from "./IServer";
import express, {Express, Request, Response} from "express";
import {ServerConnector} from "./ServerConnector";
import bodyParser from "body-parser";
import {ObjectUtil} from "../utils/ObjectUtil";
import {injectable} from "../ioc/decorators";

@injectable
export class ExpressServer implements IServer{

    private readonly server: Express;

    constructor(){
        this.server = express();
        this.server.use(bodyParser.json());
    }

    listen(serverConnector: ServerConnector): void {
        try{
            this.server.listen(serverConnector.port, () => {
                console.log(`app running on port ${serverConnector.port}`);
            })
        } catch (err) {
            throw err;
        }
    }

    route(data: { url: string, httpMethod: string, callback:(req: Request, res: Response)=> Promise<any>} ): void {

        if(!ObjectUtil.isComplete(data)){
            return
        }

        const httpMethod = data.httpMethod.toLowerCase() as any as unknown;
        const func = ObjectUtil.getObjectValueByKey(this.server, httpMethod);
        func.call(this.server, data.url, data.callback);
        console.log(httpMethod);
    }


}
