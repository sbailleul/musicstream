export interface IService {
    getOne(id: any): Promise<any>

    getAll(query: any): Promise<any>

    insert(data: any): Promise<any>

    update(id: any, data: any): Promise<any>

    delete(id: any): Promise<any>
}