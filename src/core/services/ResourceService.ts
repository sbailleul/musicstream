import {IResourceService} from "./IResourceService";
import {injectable, injected} from "../../common/ioc/decorators";
import {IResourceRepository} from "../../domain/repositories/IResourceRepository";
import {InjectableTypes} from "../../common/ioc/InjectableTypes";
import {CoreContainer} from "../../common/ioc/CoreContainer";

@injectable
export class ResourceService implements IResourceService {


    @injected(InjectableTypes.IResourceRepository)
    protected _repository: IResourceRepository;

    constructor() {
        this.getAll = this.getAll.bind(this);
        this.getOne = this.getOne.bind(this);
        this.insert = this.insert.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
        CoreContainer.injectProperties(this);
    }

    async getOne(id: any): Promise<any> {
        return this._repository.getOne(id);
    }

    async getAll(query: any): Promise<any> {
        return this._repository.getAll(query);
    }

    async insert(data: any): Promise<any> {
        return this._repository.insert(data);
    }

    async update(id: any, data: any): Promise<any> {
        return this._repository.update(id, data);
    }

    async delete(id: any): Promise<any> {
        return this._repository.delete(id);
    }
}
